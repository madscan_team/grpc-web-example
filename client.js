const grpc = require('@grpc/grpc-js');
const { HelloRequest, HelloReply } = require('./services/helloworld_pb.js');
const { GreeterClient } = require('./services/helloworld_grpc_pb.js');

const client = new GreeterClient(
  `localhost:9090`,
  grpc.credentials.createInsecure()
 )

var request = new HelloRequest();
request.setName('World');

client.sayHello(request, {}, (err, response) => {
  console.log(response.getMessage());
});


